const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";


module.exports.createAccessToken = (user) => {
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's sigh method
	// generates the token using the form data, and the secret code with ni additional options provided
	return jwt.sign(data, secret, {})
}


module.exports.verify = (req, res, next) => {
	//The token is retrieved form the request header

	let token = req.headers.authorization;

	//token received and not undefined
	if(typeof token !== "undefined") {
		console.log(token)
		//The token sent is type of "bearer" token which contains the "Bearer" as a prefix to the string
			//Syntax: string.slice(start, end)
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err,data) => {

			if(err) {
				return res.send({auth: "failed"})
			}else {
				next()
			}
		})

	} else {
		return res.send({auth: "failed"})
	}
}



module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}