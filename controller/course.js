const Course = require("../models/course");


module.exports.addCourse = async (data, reqBody) => {

		let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
	});



	if(data.isAdmin === true) {

		return newCourse.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			};
				
		})


	} else {

		return (`Not Authorized.`)
	
	 };
}

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result;
	})
}


module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.archiveCourse = async (data, reqParams, reqBody) => {

	let achivedCourse = {
		isActive: reqBody.isActive
	};

	if(data.isAdmin === true) {

		return Course.findByIdAndUpdate(reqParams.courseId, achivedCourse).then((course, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})

	} else {

		return (`Access Denied.`)
	}



}