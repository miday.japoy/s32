const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const PORT = process.env.PORT || 4000;

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())


mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.nteyu.mongodb.net/course-booking?retryWrites=true&w=majority', 
	{

		useNewUrlParser: true,
		useUnifiedTopology: true
 	
	}
)
let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log('Connected to MongoDB'))

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(PORT, () => console.log(`Local host running at port:${PORT}`))