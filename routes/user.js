const express = require("express");
const router = express.Router();
const userController = require("../controller/user")
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController =>
		res.send(resultFromController))
})


router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
})

//For enrolling a user
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	};
 
	userController.enroll(data, userData).then(resultFromController => res.send(resultFromController));
	
})


module.exports = router;